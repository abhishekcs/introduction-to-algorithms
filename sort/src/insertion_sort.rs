pub fn sort<T: Ord + Copy>(input: &mut Vec<T>) {
    for j in 1..input.len() {
        let value = input[j];
        let mut i = j - 1;
        while i < usize::MAX && input[i] > value {
            input[i + 1] = input[i];
            i = i.wrapping_sub(1)
        }

        input[i.wrapping_add(1)] = value;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sort() {
        let data = vec![
            (vec![], vec![]),
            (vec![1], vec![1]),
            (vec![1, 2, 3, 4, 5], vec![1, 2, 3, 4, 5]),
            (vec![1, 3, 2, 4, 5], vec![1, 2, 3, 4, 5]),
            (vec![3, 2, 1, 4, 5], vec![1, 2, 3, 4, 5]),
            (vec![5, 4, 3, 2, 1], vec![1, 2, 3, 4, 5]),
        ];

        for (input, output) in data {
            let mut input = input.clone();
            sort(&mut input);
            assert_eq!(input, output);
        }
    }
}
